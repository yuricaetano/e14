from calculator.addition import sum   
from calculator.subtraction import subtract   

def calculator(operation, val1, val2):
    if (operation == 'sum'):
        return sum(val1, val2)    
    if (operation == 'subtract'):
        return subtract(val1, val2)
    return "Não encontrado, por favor, use 'sum' ou 'subtract'"
